package com.simplyindexedlogger.logger;

import java.io.IOException;
import java.util.List;
import org.junit.Test;

import com.simplyindexedlogger.logger.Helper;
import com.simplyindexedlogger.logger.IndexPageReader;
import com.simplyindexedlogger.logger.Page;

/**
 * Most of these unit tests are dead, should be cleaned -up
 * @author arifkhan9
 *
 */

public class IndexPagesTest
{
  static String destinationFolder = "/Users/arifkhan9/javatest";
  
  @Test
  public void getRecentFile() throws IOException
  {
    List<String> lst = Helper.getRecentFilePair(destinationFolder);
    lst.forEach(f -> System.out.println(f));
  }
  
  @Test
  public void getIndexPages() throws IOException
  {
    String fileName = "/Users/arifkhan9/javatest/SILINDX_1571373227524.bin";
    List<Page> pages = IndexPageReader.forcedFullRead(fileName);
    for(Page dp : pages)
    {
      dp.print(); 
    }
  }
  
  @Test
  public void writeIndexPages() throws IOException
  {
    List<String> files = Helper.getRecentFilePair(destinationFolder);
    String indxFileName = destinationFolder + "/" + files.get(1);
    Iterable<Page> indxPages = IndexPageReader.forcedFullRead(indxFileName);
    String indxContents = destinationFolder + "/indxcontents.txt";
    IndexPageReader.write(indxPages, indxContents);
  }
  
  @Test
  public void IndexSearchSamePage() throws IOException
  {
    List<String> files = Helper.getRecentFilePair(destinationFolder);
    String indxFileName = destinationFolder + "/" + files.get(1);
    long eStart = 1574930155651L;
    long eEnd =   1574930156430L;
    IndexPageReader.getDataPageOffsets(indxFileName, eStart, eEnd);
    
  }

}
