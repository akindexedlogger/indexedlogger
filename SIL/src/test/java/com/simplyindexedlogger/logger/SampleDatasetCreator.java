package com.simplyindexedlogger.logger;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
//import org.junit.jupiter.api.Test;
import org.junit.Test;

import com.simplyindexedlogger.logger.DataPageReader;
import com.simplyindexedlogger.logger.Driver;
import com.simplyindexedlogger.logger.IndexPageReader;
import com.simplyindexedlogger.logger.Page;

/**
 * Please use these unit tests to see how the SIL(Simply Index Logger) works
 * after finishing rest of the code, we'll upload jar to maven
 * @author arifkhan9
 *
 */
public class SampleDatasetCreator 
{

  static String destinationFolder = "/Users/arifkhan9/javatest/";
  
 /**
  * Creates sample files 
  */
  @Test
  public void sampleFiles()
  {
    long start = System.currentTimeMillis();
    // replace the path
    try {
    Driver d = Driver.getInstance(destinationFolder);
    long i = 0;
    String t = Long.toString(i) +"--毎広暮象念立旅写医第雪生宮額趙\n" + "haha";
    while (i<1000000L) {
      d.write(t);
      i++;
    }
      d.close();
  } catch (Exception e) {
  // TODO Auto-generated catch block
    e.printStackTrace();
}
    long end = System.currentTimeMillis();
    String s = String.format("Time taken to write: %d", end - start);
    System.out.println(s);
  }

  /**
   * run this unit test to write index contents to indxcontents.txt in text format 
   * @throws IOException
   */
  
  @Test
  public void writeIndexPages() throws IOException
  {
    String indexFileNameWithFullPath = destinationFolder + "/" + "SILINDX_20191117044725_1573994845615.idx";
    Iterable<Page> indxPages = IndexPageReader.forcedFullRead(indexFileNameWithFullPath);
    String indxContents = destinationFolder + "/indxcontents.txt";
    IndexPageReader.write(indxPages, indxContents);
  }
  
  
  /**
   * Use this test to read data page contents
   * coding is in progress automate read part
   * To verify how process works, do following steps manually
   * 1) Write data to file using unit test SampleFiles()
   * 2) Verify *.txt & *.idx files are created in destination folder
   * 3) *.txt & *.idx have unix epoc timestamp as suffix, this is when files were created
   * 4) Run unit test writeIndexPages() to see contents of index. 
   * Note:
   *    Each index page has multiple records, each record has pointer to data page along with timestamp range.
   *    SIL adds timestamp to each entry in the file, page pointer & timestamp ranges are used to select only the 
   *    required data pages
   * 5) Select any epoc range from file generated in step (4)
   * 6) Update following variables in DataReaderTest() unit test
   *    dataFileNameWithFullPath -> give full path of the file ending with *.txt
   *    eStart, eEnd -> set epocStart & epocEnd values from indxcontexts.txt, values can span multiple pages. 
   *    Note: eStart should be less or equal to eEnd
   *   
   *    Run the unit test, you can update the unit test to write to the file
   * 
   * @throws Exception
   */
  
  @Test
  public void DataReaderTest() throws Exception {
    String dataFileName = "SILDATA_20191128003552_1574930152481.txt";
    String dataFileNameWithFullPath = destinationFolder +  "/" + dataFileName;
    String resultFileWithFullPath = destinationFolder + "/" + "checkdata.txt";
    long eStart = 1574930155725L;
    long eEnd =   1574930156430L;

      Iterable<String> result = DataPageReader.read(dataFileNameWithFullPath, eStart, eEnd, resultFileWithFullPath); 
  }
}
