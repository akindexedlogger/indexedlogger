package com.simplyindexedlogger.logger;

public interface ExceptionCallback {
	public void addListener(ExceptionListener listener);
	public void notifyListeners(Iterable<ExceptionListener> listeners, Exception occurredException);
}
