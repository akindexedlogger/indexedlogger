package com.simplyindexedlogger.logger;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;

/**
 * 1) Writes data to data file in pages
 * 2) Prefixes each log entry with timestamp
 * 3) When a data page is full, updates index page
 * @author arifkhan9
 *
 */

public class DataPageWriter 
{
  
  BufferedWriter writer = null;
  Page currentPage = null;
  IndexPageWriter indexWriter = null;
  
  public DataPageWriter(String destinationFolder) throws IOException 
  {
    UnixTimestamp cdt = new UnixTimestamp();
    // create new page to track data boundaries
    // these details are used to create index
    currentPage = new Page(0, 1);
    indexWriter = new IndexPageWriter(destinationFolder, cdt);
    initStreamWriter(destinationFolder, cdt);
  }

  
 private void initStreamWriter(String destinationFolder, UnixTimestamp epoc) throws IOException
 {
   File file = Helper.createFile(destinationFolder, FileType.DATAFILE, epoc);
   this.writer = new BufferedWriter(
       new OutputStreamWriter(new FileOutputStream(file,true), Helper.ENCODING));
 }
 
 private boolean isPageFull()
 {
   return currentPage.getLength() >= Helper.MAXDATAPAGESIZE;
 }
 
 public void updatePageOffset(long byteCount , long epocDate) 
 {
   currentPage.updateLength(byteCount);
   currentPage.setEpocEnd(epocDate);
   if(currentPage.getEpocStart() == -1)
   {
     currentPage.setEpocStart(epocDate);
   }
 }
 
 public void putData(String data) throws IOException, InterruptedException 
 {
   UnixTimestamp cDt = new UnixTimestamp();
   if(isPageFull())
   {
     System.out.println("Data page full");
     // update index for the current page
     indexWriter.putData(currentPage);
     // flush index page
     currentPage = new Page(currentPage.getOffset() + currentPage.getLength(), currentPage.getPageNumber()); 
   }
   // write indexer timestamp
   writer.write(cDt.epocMillisLineIndicator);
   // write tab
   writer.write(Helper.TABSTRING);
   // write actual data
   writer.write(data);
   // write new line
   writer.write(System.lineSeparator());
   
   // compute total bytes written 
   long byteCount = Helper.getByteCount(cDt.epocMillisLineIndicator) 
                 + Helper.getByteCount(data) 
                 + Helper.RECORDMETABYTECOUNT ;

   updatePageOffset(byteCount, cDt.epocMillis);
 }
 
 public void close() throws IOException
 {
   // update index
   indexWriter.putData(currentPage);
   Helper.closeWriter(writer);
   indexWriter.close();
 }

}
