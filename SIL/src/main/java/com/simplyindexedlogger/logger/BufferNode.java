package com.simplyindexedlogger.logger;

/**
 * @Description Node holding log entry, this becomes part of buffer collection
 * @author arifkhan9
 *
 */
public class BufferNode 
{
	final String logEntry;
	BufferNode next;
	BufferNode previous;

	public BufferNode(String dataItem) 
	{
		this.logEntry = dataItem;
		this.next = null;
		this.previous = null;
	}
}
