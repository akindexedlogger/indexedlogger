package com.simplyindexedlogger.logger;

import java.io.IOException;
/**
 * Class for client interaction
 * @author arifkhan9
 *
 */

public class Driver implements ExceptionListener{

	private static Driver driverInstance;
	public synchronized static Driver getInstance(String targetLocation) throws IOException 
	{
		if(driverInstance == null) 
		{
			driverInstance = new Driver(targetLocation);
		}
		return driverInstance;
	}
	
	// Instance members
	private final Buffer sharedBuffer;
	private final DestinationWriterThread dWriterThread;
	
	private Driver(String targetLocation) throws IOException 
	{
		this.sharedBuffer = Buffer.getInstance();
		this.dWriterThread = new DestinationWriterThread(targetLocation);
		this.dWriterThread.addListener(this);
		dWriterThread.start();
	}
	
	public void write(String dataItem) 
	{
		sharedBuffer.Add(dataItem);
	}

	public void close() 
	{
		try 
		{
		  long start = System.currentTimeMillis();
			while(this.sharedBuffer.Exists())
			{
			  Thread.sleep(Helper.DEFAULTSLEEPINTERVAL);
			  long end = System.currentTimeMillis();
			  if((end - start) >= Helper.MILLISTOWAITBEFORETERMINATION)
			  {
			    String s = String.format("End:%d Start:%d Elapsed:%d", end, start, end - start);
			    System.out.println(s);
			    break;
			  }
			}
			System.out.println("PLEASE HOLD ON, PROCESSING IS FLUSHING DATA TO DISK");
			// data successfully written to disk
			if(!this.sharedBuffer.Exists())
      {
        System.out.println("");
      }
			else
			{
			  String s = String.format("NodeAddedCount:%d NodeRemovedCount:%d", this.sharedBuffer.getAddedCount(), this.sharedBuffer.getRemovedCount());
			  System.out.println(s);
			  System.out.println("Thread interrupted abruptly, may result poteantial loss of data & index corruption");
			}
			dWriterThread.interrupt();
			Thread.sleep(Helper.DEFAULTSLEEPINTERVAL);
		} catch (InterruptedException e) 
		{
		}
		
	}

	// Exception callbacks  
	@Override
	public void exceptionOccurred(Exception exception)
	{
		System.out.println("THIS IS FROM CALLBACK");
//		throw new RuntimeException(exception);
	}
	
}
