package com.simplyindexedlogger.logger;

/**
 * @Description This is the main buffer between writer & reader threads, it works as a queue
 * Writer writes to one end of the buffer while buffer reads from other end
 * @author arifkhan9
 *
 */
public class Buffer 
{
	private final Object lock = new Object();
	
	private Buffer() {}

	private static Buffer collectorInstance;
	public static synchronized Buffer getInstance() 
	{
		if(collectorInstance == null) 
		{
			collectorInstance = new Buffer();
		}
		return collectorInstance;
	}
	private BufferNode head;
	private BufferNode tail;
	private int itemCount;
	private int removedCount = 0;
	
	public Object getLock() 
	{
		return lock;
	}

	public void Add(String dataItem) 
	{
		// critical section
		synchronized(lock)
		{
			BufferNode newNode = new BufferNode(dataItem);
			if(head == null) 
			{
			    head = newNode;
			    tail = head;
			}
			else 
			{
				newNode.next = head;
				head.previous = newNode;
				head = newNode;
			}
			lock.notifyAll();
			itemCount++;
		}
		System.out.println(itemCount);
	}
	
	public  boolean Exists() 
	{
		return head != null;
	}
	
	public String Remove() throws InterruptedException 
	{
		String item = tail.logEntry;
		synchronized(lock) 
		{
		  if(head==tail)
		  {
		      head=null;
		      tail=null;
		  }
		  else 
		  {
		    BufferNode temp = tail;
		    tail = tail.previous;
		    tail.next = null;
		    temp.previous = null;
		  }
		  removedCount++;
		}
		return item;
	}

	public BufferNode getHead() 
	{
		return head;
	}
	
	public BufferNode getTail() 
	{
		return tail;
	}
	
	public int getAddedCount() 
	{
		return itemCount;
	}
	
	public int getRemovedCount()
	{
	  return removedCount;
	}
}
