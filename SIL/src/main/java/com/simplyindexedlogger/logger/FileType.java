package com.simplyindexedlogger.logger;
/**
 * Index or Data file types
 * @author arifkhan9
 *
 */
public enum FileType
{
  DATAFILE,
  INDEXFILE
}
