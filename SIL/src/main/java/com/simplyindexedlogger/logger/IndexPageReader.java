package com.simplyindexedlogger.logger;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Set;

/**
 * Here's how index pages are designed...  
 * 1) Index page size if fixed (8KB)
 * 2) Each record in the index page is 32 bytes in size
 * 3) First record holds metadata of the page itself 
 *      i.e; number of records in the page, min(timestamp of data pages), max(timestamp of the data pages) 
 * 4) Rest of the records contains pointers to data pages, here is how data page pointer looks like
 *    32 bytes record:  first 8 bytes records page offset in the data file, 
 *                      second 8 bytes number of bytes from offset (length) 
 *                      third 8 bytes start timestamp of log entries in the data page
 *                      last 8 bytes end timestamp of log entries in the date page
 * 5) In total each index page has 254 records, 253 records points to data page offsets.
 * 6) First record is used to do binary search and rest of the records are used to select data pages
 * 
 * Index files have data in sorted order with fixed size pages & fixed size records
 * Binary search is used to select index page, select data page offsets for the timestamp range 
 * @author arifkhan9
 *
 */
public class IndexPageReader
{
  private static long getPageCount(long fileSize) throws IOException
  {
    if(fileSize % Helper.MAXINDXPAGESIZE !=0 )
    {
      assert (fileSize % Helper.MAXINDXPAGESIZE !=0) : "Index page size is not rounded to record boudaries";
    }
    return fileSize / Helper.MAXINDXPAGESIZE;
  }
  
  /**
   * pageNumber starts from ZERO
   * @param pageNumber
   * @param pageBuffer
   * @throws IOException
   */
  private static void getPageBytes(long pageNumber, byte[] pageBuffer, RandomAccessFile raf) throws IOException
  {
      long seekPosition = pageNumber * Helper.MAXINDXPAGESIZE;
      raf.seek(seekPosition);
      raf.read(pageBuffer, 0, Helper.MAXINDXPAGESIZE);
  }
 
  private static IndexPageMetadata getIndexPageMetadata(byte[] pageBuffer, long pageNumber)
  {
    ByteBuffer byteBuffer = ByteBuffer.wrap(pageBuffer);
    int recordCount = byteBuffer.getInt();
    long epocStart = byteBuffer.getLong();
    long epocEnd = byteBuffer.getLong();
    return new IndexPageMetadata(recordCount, epocStart, epocEnd, pageNumber, pageBuffer);
  }
  
  private static void getPageRecords(byte[] pageBuffer, List<Page> listToAppend, long pageNumber, int pageBufferOffset, int pageBufferLength)
  {
    ByteBuffer bb = ByteBuffer.wrap(pageBuffer);
    int recordCount = bb.getInt();
    long epocStart = bb.getLong();
    long epocEnd = bb.getLong();
    bb.position(IndexPageWriter.firstRecordOffset);
    for(int i = 0; i < recordCount; i++)
    {
      Page dp = new Page(bb, pageNumber, recordCount);
      if(dp.getEpocStart() < epocStart || dp.getEpocEnd() > epocEnd)
      {
        String error = String.format("Pointers to DataPage is not in  expected range, Actual [%d %d], Expected [%d %d]", dp.getEpocStart(), dp.getEpocEnd(), epocStart, epocEnd);
        throw new RuntimeException(error);
      }
      listToAppend.add(dp);
    }
  }
 
  public static List<Page> forcedFullRead(String fileNameWithFullPath) throws IOException
  {
    RandomAccessFile raf = null;
    List<Page> pages = null;
    try
    {
      raf = new RandomAccessFile(fileNameWithFullPath,"r");
      raf.seek(0);
      pages = new LinkedList<Page>();
      long fileSize = Helper.getFileSize(fileNameWithFullPath);
      long numberOfPages = getPageCount(fileSize);
      byte[] pageBuffer = new byte[Helper.MAXINDXPAGESIZE];

      for(int i = 0; i < numberOfPages ; i++)
      {
        long seekPosition = i * Helper.MAXINDXPAGESIZE;

        raf.seek(seekPosition);
        raf.read(pageBuffer, 0, Helper.MAXINDXPAGESIZE);
    
        getPageBytes(i, pageBuffer, raf);
        getPageRecords(pageBuffer, pages, i + 1, 0, pageBuffer.length);
      }
    }
    finally
    {
      Helper.closeRandomAccessFile(raf);
    }
    return pages;
  }
 
  /** NOTE: This code need re-factoring, should be moved to common method
   * Scan index for small index file
   * @param fileNameWithFullPath
   * @param epocStart
   * @param epocEnd
   * @return
   * @throws IOException
   */
  private static Iterable<Page> doIndexScan(String fileNameWithFullPath, long epocStart, long epocEnd) throws IOException
  {
    RandomAccessFile raf = null;
    List<Page> pages = null;
    try
    {
      raf = new RandomAccessFile(fileNameWithFullPath,"r");
      raf.seek(0);
      pages = new LinkedList<Page>();
      int  fileSize = (int) Helper.getFileSize(fileNameWithFullPath);
      long numberOfPages = getPageCount(fileSize);
      byte[] fileBuffer = new byte[fileSize];
      raf.read(fileBuffer);
      ByteBuffer byteBuffer = ByteBuffer.wrap(fileBuffer);
      List<Page> dataPageOffsets = new LinkedList<Page>();
      int dataPageNumber = 0;

      for(int i = 0; i < numberOfPages ; i++)
      {
        int seekPosition = i * Helper.MAXINDXPAGESIZE;
        byteBuffer.position(seekPosition);
        int recordCount = byteBuffer.getInt();
        long eStart = byteBuffer.getLong();
        long eEnd = byteBuffer.getLong();

        if(!inTheRange(eStart, eEnd, epocStart, epocEnd))
        {
          continue;
        }
        // seek to first data page pointers record within index page
        byteBuffer.position(seekPosition + IndexPageWriter.firstRecordOffset);
        for(int j = 0; j < recordCount; j++)
        {
          // record count in data page pointer is not being used, it's not holding data record counts in data pages
          // we should look into removing recordCount parameter , it's freaking confusing to have it here (done by me :))
          Page dp = new Page(byteBuffer, dataPageNumber, recordCount);
          dataPageOffsets.add(dp);
        }
      }
    }
    finally
    {
      Helper.closeRandomAccessFile(raf);
    }
    return pages;
  }
 
  
  private static void searchIndexPages(RandomAccessFile raf
                                  , long low
                                  , long high
                                  , long epocStart
                                  , long epocEnd
                                  , PriorityQueue<IndexPageMetadata> pq
                                  , Set<Long> processedPages) throws IOException
  {
    // search boundary check
    if(low>high)
    {
      return;
    }
    long currentPage = (low + high) /2;
    // page already processed, return
    if(processedPages.contains(currentPage))
    {
      return;
    }

    // buffer to hold index page contents 
    byte[] pageBuffer = new byte[Helper.MAXINDXPAGESIZE];
    // get index page content
    getPageBytes(currentPage, pageBuffer, raf);
    // get first record (metadata) from index page, processing decision is based on this record
    IndexPageMetadata currentPageMetadata = getIndexPageMetadata(pageBuffer, currentPage);

    // check if we need to process current page
    if(currentPageMetadata.contains(epocStart, epocEnd))
    {
      // collect data and mark page as processed
      pq.add(currentPageMetadata);
      processedPages.add(currentPage);
    }
    
    // should proceed left
    if(currentPageMetadata.epocStart >= epocStart)
    {
      searchIndexPages(raf, low, currentPage - 1, epocStart, epocEnd, pq, processedPages);
    }
    // should proceed right
    if(currentPageMetadata.epocEnd <= epocEnd)
    {
      searchIndexPages(raf, currentPage + 1, high, epocStart, epocEnd, pq, processedPages);
    }
  }
 
  
  public static Iterable<Page> getDataPageOffsets(String fileNameWithFullPath, long epocStart, long epocEnd) throws IOException
  {
    long fileSize = Helper.getFileSize(fileNameWithFullPath);
    
    // as of now, only binary search is tested, full file read need testing, until then,
    // to force binary search even on small files, MAXINDEXFILESIZETOSCAN is set to very low value
    // read all content at once for small files
    if(fileSize <= Helper.MAXINDEXFILESIZETOSCAN)
    {
      return doIndexScan(fileNameWithFullPath, epocStart, epocEnd);
    }
    // index search
    else {
      return doIndexSeek(fileNameWithFullPath, epocStart, epocEnd);
      
    }
  }
  
  private static Iterable<Page> doIndexSeek(String fileNameWithFullPath, long epocStart, long epocEnd) throws IOException
  {
    try(RandomAccessFile raf = new RandomAccessFile(fileNameWithFullPath, "r"))
    {
      long fileSize = Helper.getFileSize(fileNameWithFullPath);
      long pageCount = getPageCount(fileSize);
      long low = 0;
      long high = pageCount -1;
      PriorityQueue<IndexPageMetadata> pq = new PriorityQueue<IndexPageMetadata>();
      HashSet<Long> processedPages = new HashSet<Long>();
      // search thru the index and get results in PriorityQueue
      searchIndexPages(raf, low, high, epocStart, epocEnd, pq, processedPages);    
      Iterable<Page> dataPageOffsets = readDataPageOffsets(pq, epocStart, epocEnd);
      return dataPageOffsets;
    }
  }
  
  private static boolean inTheRange(long sourceEStart, long sourceEEnd, long targetEStart, long targetEEnd)
  {
    return ((targetEStart >= sourceEStart) && (targetEStart <= sourceEEnd)) ? true : false;
  }

  private static void getPageRecords(byte[] pageBuffer, List<Page> listToAppend, long pageNumber, long targetEStart, long targetEEnd)
  {
    ByteBuffer bb = ByteBuffer.wrap(pageBuffer);
    int recordCount = bb.getInt();
    long epocStart = bb.getLong();
    long epocEnd = bb.getLong();
    
    // no need to process if timestamp value is not within the target range
    if(!Helper.inTheRange(epocStart, epocEnd, targetEStart, targetEEnd))
    {
      return;
    }
    
    bb.position(IndexPageWriter.firstRecordOffset);
    for(int i = 0; i < recordCount; i++)
    {
      Page dp = new Page(bb, pageNumber, recordCount);
      listToAppend.add(dp);
    }
  }  
  
  private static Iterable<Page> readDataPageOffsets(PriorityQueue<IndexPageMetadata> pq, long epocStart, long epocEnd)
  {
    List<Page> pages = new LinkedList<Page>();
    while(pq.size()>0)
    {
      IndexPageMetadata indxMeta = pq.remove();
      getPageRecords(indxMeta.indexPageBuffer, pages, indxMeta.pageNumber, epocStart, epocEnd);
    }
    return pages;
  }
  
  public static void write(Iterable<Page> pages, String filePath) throws IOException
  {
    BufferedWriter br = null;
    try     
    {
      br = new BufferedWriter(new FileWriter(filePath)); 
      for(Page page : pages) 
      {
        br.write(page.toString()); 
        br.newLine();
      }
    }
    finally 
    {
      Helper.closeWriter(br);
    }
  }
}
