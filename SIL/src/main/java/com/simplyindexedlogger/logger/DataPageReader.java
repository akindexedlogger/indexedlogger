package com.simplyindexedlogger.logger;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.CodingErrorAction;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Pattern;
import java.util.regex.Matcher;
import java.text.ParseException;
/**
 * Reads data for the given range of timestamps 
 * @author arifkhan9
 *
 */
public class DataPageReader {
 
  // Lot of scope to optimize this code
  public static Iterable<String> read(String dataFileNameWithFullPath, long startUnixEpocInMillis, long endUnixEpocInMillis, String resultFilePath) throws IOException, ParseException
  {
    RandomAccessFile raf = null;
    List<String> result = new LinkedList<String>();
    try
    {
      String indxFileNameWithFullPath = Helper.getIndexFileNameWithFullPath(dataFileNameWithFullPath);
      Iterable<Page> indices = IndexPageReader.getDataPageOffsets(indxFileNameWithFullPath, startUnixEpocInMillis, endUnixEpocInMillis);
      raf = new RandomAccessFile(dataFileNameWithFullPath, "r");
      for(Page page: indices)
      {
        if(!Helper.inTheRange(page.getEpocStart(), page.getEpocEnd(), startUnixEpocInMillis, endUnixEpocInMillis))
          continue;
        readLines(page.getOffset(), page.getLength(), raf, startUnixEpocInMillis, endUnixEpocInMillis, resultFilePath); 
      }
    }
    finally
    {
      Helper.closeRandomAccessFile(raf);
    }
    return result;
  }
  
  
  private static void readLines(long offset, long length, RandomAccessFile raf,long sEpoc, long eEpoc, String resultFilePath) throws IOException, ParseException
  {
    byte[] buffer = new byte[(int)length];
    raf.seek(offset);
    raf.read(buffer);
    CharsetDecoder csd = Charset.forName(Helper.ENCODING.toString()).newDecoder();
    csd.onMalformedInput(CodingErrorAction.REPORT);
    csd.onUnmappableCharacter(CodingErrorAction.REPLACE);
    String sourceData = csd.decode(ByteBuffer.wrap(buffer)).toString();
    String patternString = "(<\\d{13}>\\t)";
    Pattern pattern = Pattern.compile(patternString);
    Matcher matcher = pattern.matcher(sourceData);
    
    int startMatchPosition = -1;
    int endMatchPosition = sourceData.length();
    boolean matchStarted = false;
    while(matcher.find())
    {
        boolean res = (IsLineInRange(sourceData, matcher, sEpoc, eEpoc));
        if(res)
        {
          if(startMatchPosition == -1)
          {
            matchStarted = true;
            startMatchPosition = matcher.start();
          }
        }
        else 
        {
          if(matchStarted)
          {
            endMatchPosition = matcher.start() - 1;
            break;
          }
        }
    }
    if(matchStarted)
    {
     try(FileWriter fw = new FileWriter(resultFilePath); BufferedWriter bw = new BufferedWriter(fw))
     {
       bw.write(sourceData.substring(startMatchPosition, endMatchPosition));
     }
    }
  }
  
  private static boolean IsLineInRange(String sourceData, Matcher matcher, long sEpoc, long eEpoc) throws ParseException
  {
      int start = matcher.start();
      int end = matcher.end();
      // extract datetime string
      long epoc = Long.parseLong(sourceData.substring(start+1,end-2));
      String st = String.format("%d %d %d", sEpoc, epoc, eEpoc);
      System.out.println(st);
      return (epoc>= sEpoc && epoc<=eEpoc);
  }
}
