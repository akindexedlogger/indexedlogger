package com.simplyindexedlogger.logger;

import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
/**
 * Most of the indexer logic is dependent on timestamps, hence utility class
 * @author arifkhan9
 *
 */
public final class UnixTimestamp implements Comparable<UnixTimestamp>{
	public final String utcDateTime;
	public final long epocMillis;
	public final Instant instant;
	public final String formattedTimestamp;
	public final String dataLineIndicator;
	public static final String lineIndicatorFormat = "<yyyyMMddHHmmss>";
	public final String epocMillisLineIndicator;
	
	public UnixTimestamp() {
	  
      this.instant = Instant.now().minus(14, ChronoUnit.HOURS);
      this.utcDateTime = instant.toString();
      this.epocMillis = instant.toEpochMilli();
      this.formattedTimestamp = getFormattedTimestamp(instant);
      this.dataLineIndicator = getDataLineIndicator(instant);
      this.epocMillisLineIndicator = "<" + epocMillisString() + ">";
      
	}
	
	public String epocMillisString()
	{
	 return Long.toString(epocMillis);
	}
	
	public Instant getInstant()
	{
	  return instant;
	}

	public int getMinute()
	{
	    return instant.atZone(ZoneOffset.UTC).getMinute();
	}

	public int getSecond()
	{
	      return instant.atZone(ZoneOffset.UTC).getSecond();
	}
	
	public int getHour()
	{
	  return instant.atZone(ZoneOffset.UTC).getHour();
	}
	
	public int getDay()
	{
	  return instant.atZone(ZoneOffset.UTC).getDayOfMonth();
	}
	
	public int getYear()
	{
	  return instant.atZone(ZoneOffset.UTC).getYear();
	}
	
	public int getMonth()
	{
	  return instant.atZone(ZoneOffset.UTC).getMonthValue();
	}
	
	private String getFormattedTimestamp(Instant instant)
	{
	  SimpleDateFormat df = new SimpleDateFormat(Helper.DATETIME_FORMATTER);
	  return df.format(Date.from(instant)) ;
	}
	private String getDataLineIndicator(Instant instant)
  {
	    SimpleDateFormat df = new SimpleDateFormat(lineIndicatorFormat);
	    return df.format(Date.from(instant));
	}
	
	public static int getDatalineIndicatorCharLength()
	{
	  return lineIndicatorFormat.length();
	}
	
	public String getFormattedTimestamp()
	{
	  return this.formattedTimestamp;
	}
	
  @Override
  public int compareTo(UnixTimestamp otherInstant) {
    return this.instant.compareTo(otherInstant.instant);
  }

}
