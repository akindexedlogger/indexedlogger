package com.simplyindexedlogger.logger;

public interface ExceptionListener {
	public void exceptionOccurred(Exception exception);
}
