package com.simplyindexedlogger.logger;

/**
 * Index page metadata includes following items
 * 
 * Here's how index pages are designed...  
 * 1) Index page size if fixed (8KB)
 * 2) Each record in the index page is 32 bytes in size
 * 3) First record holds metadata of the page itself 
 *      i.e; number of records in the page, min(timestamp of data pages), max(timestamp of the data pages) 
 * 4) Rest of the records contains pointers to data pages, here is how data page pointer looks like
 *    32 bytes record:  first 8 bytes records page offset in the data file, 
 *                      second 8 bytes number of bytes from offset (length) 
 *                      third 8 bytes start timestamp of log entries in the data page
 *                      last 8 bytes end timestamp of log entries in the date page
 * 5) In total each index page has 254 records, 253 records points to data page offsets.
 * 6) First record is used to do binary search and rest of the records are used to select data pages
 * @author arifkhan9
 *
 */

public class IndexPageMetadata implements Comparable<IndexPageMetadata>{
  public final int recordCount;
  public final long epocStart;
  public final long epocEnd;
  public final long pageNumber;
  public final byte[] indexPageBuffer;
  
  public byte[] getIndexPageBuffer() {
    return indexPageBuffer;
  }

  public IndexPageMetadata(int recordCount, long epocStart, long epocEnd, long pageNumber, byte[] pageBuffer)
  {
    this.recordCount = recordCount;
    this.epocStart = epocStart;
    this.epocEnd = epocEnd;
    this.pageNumber = (int)pageNumber;
    this.indexPageBuffer = pageBuffer;
  }
  
  // epoc within the range
  // if true, index page contains offsets to data pages for the targeted timestamp`:w
  public boolean contains(long eStart, long eEnd)
  {
      return Helper.inTheRange(this.epocStart, this.epocEnd, eStart, eEnd);
  }
  
  public boolean isLarger(long epoc)
  {
    return (epoc > epocEnd) ? true : false;
  }
  
  public boolean isSmaller(long epoc)
  {
    return (epoc < epocStart) ? true : false;
  }


  @Override
  public int compareTo(IndexPageMetadata o) {
    if(this.pageNumber < o.pageNumber)
      return -1;
    if(this.pageNumber > o.pageNumber)
      return 1;
    return 0;
}
}
