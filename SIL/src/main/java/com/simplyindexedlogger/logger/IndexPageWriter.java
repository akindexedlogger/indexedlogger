package com.simplyindexedlogger.logger;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.io.RandomAccessFile;
import java.io.File;

/**
 *Here's how index pages are designed...  
 * 1) Index page size if fixed (8KB)
 * 2) Each record in the index page is 32 bytes in size
 * 3) First record holds metadata of the page itself 
 *      i.e; number of records in the page, min(timestamp of data pages), max(timestamp of the data pages) 
 * 4) Rest of the records contains pointers to data pages, here is how data page pointer looks like
 *    32 bytes record:  first 8 bytes records page offset in the data file, 
 *                      second 8 bytes number of bytes from offset (length) 
 *                      third 8 bytes start timestamp of log entries in the data page
 *                      last 8 bytes end timestamp of log entries in the date page
 * 5) In total each index page has 254 records, 253 records points to data page offsets.
 * 6) First record is used to do binary search and rest of the records are used to select data pages 
 * 
 * This class is used from data writer, data writer updates index page when data page is full
 * @author arifkhan9
 *
 */
public class IndexPageWriter
{

  private ByteBuffer buffer;
  private RandomAccessFile writer;
  public static final int firstRecordOffset = 32; 
  public static final int metadataOffset = 0;
  private Page currentPage = null;
  public static final int recordSize = 32;
  
  public IndexPageWriter(String destinationFolder, UnixTimestamp epoc) throws IOException 
  {
    createNewPage((long) 0, (long) 0, epoc.epocMillis);
    initBuffer();
    initStreamWriter(destinationFolder, epoc);
  }
  
  
  private void createNewPage(long offset, long pageNumber, long epoc)
  {
    currentPage = new Page(offset, pageNumber);
    currentPage.setEpocStart(epoc);
  }
  
  
  /**
   * Index pages are in fixed size of 8KB each
   * first 32 bytes are reserved for metadata: first 4 bytes gives row count, rest are reserved for future usage
   * each record is in fixed size of 32 bytes
   * 
   */
  private void initBuffer()
  {
    buffer = ByteBuffer.allocate(Helper.MAXINDXPAGESIZE);
    buffer.position(firstRecordOffset);
  }
  
  private void initStreamWriter(String destinationFolder, UnixTimestamp epoc) throws IOException
  {
    File file = Helper.createFile(destinationFolder, FileType.INDEXFILE, epoc);
    writer = new RandomAccessFile(file, "rws");
  }
  
  private boolean isPageFull()
  {
    return buffer.position() == Helper.MAXINDXPAGESIZE; 
  }
  
  public void putData(Page dataPage) throws IOException
  {
    putData(dataPage.getOffset(), dataPage.getLength(), dataPage.getEpocStart(), dataPage.getEpocEnd());
  }
  
  public void putData(long dataPageOffset, long dataPageLength, long epocStart, long epocEnd) throws IOException
  {
    if(isPageFull())
    {
      flush();
      // initialize buffer for new page
      initBuffer();
      // create new page
      // new page offset starts at currentPage offset + index page size
      long newPageOffset = currentPage.getOffset() + Helper.MAXINDXPAGESIZE;
      createNewPage(newPageOffset, this.currentPage.getPageNumber() + 1, epocStart);
      
    }
    // write data page pointers
    buffer.putLong(dataPageOffset);
    buffer.putLong(dataPageLength);
    buffer.putLong(epocStart);
    buffer.putLong(epocEnd);

    // update current page with values needed in page metadata 
    currentPage.incrementRecordCount();
    currentPage.updateLength(recordSize, Helper.MAXINDXPAGESIZE);
    currentPage.setEpocEnd(epocEnd);
}

  private void updatePageMetaData()
  {
    // update page metada
    buffer.position(metadataOffset);
    // Record count is required when page is partially fully
    buffer.putInt(currentPage.getRecordCount());
    // epocStart & epocEnd times gives the range of data pages pointed to in the index page
    // skip next 8 bytes, epocStart was written in there when page was created
    // not skipping these 8 bytes will overwrite data
    buffer.putLong(currentPage.getEpocStart());
    buffer.putLong(currentPage.getEpocEnd());
  }
  
 /**
  * Index pages are fixed size of 8KB each 
  * Seek to page boundary to flush to the disk 
 * @throws IOException 
  */
  private void flush() throws IOException
  {
    if(this.currentPage.recordCount == 0)
    {
      return;
    }
    updatePageMetaData();
    long seekPosition = (this.currentPage.getPageNumber() ) * Helper.MAXINDXPAGESIZE;
    writer.seek(seekPosition);
    writer.write(buffer.array());
  }
 
  public long getPageNumber()
  {
    return currentPage.getPageNumber();
  }
  
  public void close()  
  {
    try
    {
      flush();
      
    } catch (IOException e1)
    {
    }
    finally {
      try
      {
        writer.close();
      } catch (IOException e)
      {
      }
    }
  }
}
