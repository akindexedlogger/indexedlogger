package com.simplyindexedlogger.logger;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.io.Writer;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.io.File;

/**
 * Utility class with static members
 * @author arifkhan9
 *
 */
public final class Helper {

	public static final char TAB = '\t';
	public static final String TABSTRING = "\t";
  public static final char NEWLINE = '\n';
  public static final String NEWLINESTRING = "\n";
  public static final int MAXDATAPAGESIZE = 1024 * 512;
  public static final int MAXINDXPAGESIZE = 1024 * 8;
  public static final String DATAFILENAMEPREFIX = "SILDATA_";
  public static final String INDXFILENAMEPREFIX ="SILINDX_"; 
  public static final String DATAFILEEXT = ".txt";
  public static final String INDXFILEEXT = ".idx";
  public static final Charset ENCODING = StandardCharsets.UTF_8;
  public static final int TABBYTECOUNT = TABSTRING.getBytes().length;
  public static final int NEWLINEBYTECOUNT = System.lineSeparator().getBytes().length;
  public static final int RECORDMETABYTECOUNT = TABBYTECOUNT + NEWLINEBYTECOUNT;
  public static final long MAXINDEXFILESIZETOSCAN = 1024 * 1;
  public static final long MILLISTOWAITBEFORETERMINATION = 1000 * 60 * 3;
  public static final long DEFAULTSLEEPINTERVAL = 9000L;
  public static final String DATETIME_FORMATTER = "yyyyMMddHHmmss";
  public static final String TIMEZONE = "UTC";
  
	public static void closeWriter(Writer wr) 
	{
		try 
		{
		  if(wr != null)
		  {
		    wr.close();
		  }
		} catch (Exception e) {}
	}
	
	public static void closeRandomAccessFile(RandomAccessFile raf)
	{
	  try
	  {
	    if(raf != null)
	    {
	      raf.close();
	    }
	  }
	  catch(Exception ex) {}
	}
	
	public static int getByteCount(String dataItem)
	{
	  return dataItem.getBytes(Helper.ENCODING).length;
	}
	
	public static File createFile(String destinationFolder, FileType fileType, UnixTimestamp epoc) throws IOException
	{
	  String fileNameWithPath = getDataFileName(destinationFolder, fileType, epoc);    
    File file = new File(fileNameWithPath);
    if(!file.exists()) 
    {
      file.getParentFile().mkdir();
      file.createNewFile();
      file.setExecutable(true, false);
      file.setReadable(true, false);
      file.setWritable(true, false);
    }
    return file;
	}
	
	public static String getDataFileName(String destinationFolder, FileType fileType, UnixTimestamp epoc)
	{
	  String prefix = (fileType == FileType.DATAFILE) ? DATAFILENAMEPREFIX : INDXFILENAMEPREFIX;
	  String extn = (fileType == FileType.DATAFILE) ? DATAFILEEXT : INDXFILEEXT;
	  StringBuilder sb = new StringBuilder();
	  sb.append(prefix);
	  sb.append(epoc.getFormattedTimestamp());
	  sb.append("_");
	  sb.append(epoc.epocMillisString());
	  sb.append(extn);
	  return Paths.get(destinationFolder, sb.toString()).toString();
	}
	
	public static String getFileName(String prefix, String epoc, String extn)
	{
	  return prefix + epoc + extn;
	}
	
	public static List<String> getFiles(String destinationFolder,FileType fileType) throws IOException
	{
	  String prefix = (fileType == FileType.DATAFILE) ? DATAFILENAMEPREFIX : INDXFILENAMEPREFIX;
	  @SuppressWarnings("resource")
    Stream<Path> files = Files.walk(Paths.get(destinationFolder));
	  List<String> result = files.map(f -> f.getFileName().toString())
	                              .filter(f -> f.contains(prefix))
	                              .collect(Collectors.toList());
	  return result;
	}
	
	public static long getFileSize(String fileNameWithFullPath) throws IOException
	{
	  File file = new File(fileNameWithFullPath);
	  if(!file.exists())
	  {
	    throw new IOException("Cannot get file size on non existing file");
	  }
	  return file.length();
	}

	//NOTE: THIS CODE SHOULD BE OPTIMIZED TO NOT PROCESS MORE THAN NEEDED FILES
	public static boolean singleEpocInTheRange(long sourceEpoc, long targetEpocStart, long targetEpocEnd)
	{
	  if(sourceEpoc <= targetEpocStart)
	    return true;
	  return false;
	}

	public static boolean inTheRange(long sourceEpocStart, long sourceEpocEnd, long targetEpocStart, long targetEpocEnd)
  {
	  if(targetEpocStart <= sourceEpocStart && targetEpocEnd >= sourceEpocStart)
	    return true;
	  if(targetEpocStart >= sourceEpocStart && targetEpocEnd <= sourceEpocEnd)
	    return true;
	  if(targetEpocStart >= sourceEpocStart && targetEpocStart <= sourceEpocEnd)
	    return true;
	  return false;
  }
  
	
	public static List<String> getRecentFilePair(String destinationFolder) throws IOException
	{
	  // get data files
	  List<String> dataFiles = getFiles(destinationFolder, FileType.DATAFILE);
	  // get recent data file
	  List<String> datFile = dataFiles.stream().sorted(Comparator.reverseOrder()).limit(1).collect(Collectors.toList());
	  String dataFile = datFile.get(0);
	  
	  // get index files
	  List<String> indexFiles = getFiles(destinationFolder, FileType.INDEXFILE);
	  // get recent index file
	  List<String> idxFile = indexFiles.stream().sorted(Comparator.reverseOrder()).limit(1).collect(Collectors.toList());
	  String indexFile = idxFile.get(0);
	  return Arrays.asList(dataFile,indexFile);
	}
	
	public static String getIndexFileNameWithFullPath(String dataFileNameWithFullPath)
	{
	  return dataFileNameWithFullPath.replace(DATAFILENAMEPREFIX, INDXFILENAMEPREFIX).replace(DATAFILEEXT, INDXFILEEXT);
	}
}
