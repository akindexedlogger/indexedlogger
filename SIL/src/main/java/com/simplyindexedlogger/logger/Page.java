package com.simplyindexedlogger.logger;

import java.nio.ByteBuffer;
/**
 * Container to hold data / index page information
 * Note: there is a scope to re-factor this code, particularly constructors
 * @author arifkhan9
 *
 */
public class Page
{
  protected  long offset;
  public void setOffset(long offset) {
    this.offset = offset;
  }

  protected long length;
  protected  long pageNumber;
  public void setPageNumber(long pageNumber) {
    this.pageNumber = pageNumber;
  }

  protected int recordCount;
  private long epocStart = -1;
  private long epocEnd;

  public Page(long offset, long pageNumber)
  {
    this.offset = offset;
    this.pageNumber = pageNumber;
  }
  public Page() {}
  
  public Page(ByteBuffer bb, long pageNumber, int recordCount)
  {
    this(bb.getLong(), pageNumber);
    this.length = bb.getLong();
    this.epocStart = bb.getLong();
    this.epocEnd = bb.getLong();
    this.recordCount = recordCount;
  }

  public long getEpocStart()
  {
    return epocStart;
  }

  public void setEpocStart(long epocStart)
  {
    this.epocStart = epocStart;
  }

  public long getEpocEnd()
  {
    return epocEnd;
  }

  public void setEpocEnd(long epocEnd)
  {
    this.epocEnd = epocEnd;
  }  

  public long getOffset()
  {
    return offset;
  }
  
  public long getLength()
  {
    return length;
  }
  
  public void setLength(long length)
  {
    this.length = length;
  }
 
  public void updateLength(long value)
  {
    this.length += value;
  }

  public void updateLength(long value, int maxAllowedSize)
  {
    
    String s = String.format("Index page size exceeding limit, current Length:%d new Value:%d added:%d", this.length, value, this.length + value);
    
    this.length = this.length + value;
    
    if(this.length > maxAllowedSize)
    {
      
      throw new RuntimeException(s);
    }
    System.out.println(this.length);
  }
 
  public long getPageNumber()
  {
    return pageNumber;
  }
  
  public int getRecordCount()
  {
    return recordCount;
  }
  
  public void setRecordCount(int recordCount)
  {
    this.recordCount = recordCount;
  }
  
  public void incrementRecordCount()
  {
    this.recordCount++;
  }
  
  @Override
  public String toString()
  {
    return String.format("PageNumber:%d\tRecordCount:%d\tOffset:%d\tLength:%d\tEpocStart:%d\tEpocEnd:%d", pageNumber,recordCount,offset,length,epocStart, epocEnd);
  }
  
  public void print()
  {
    System.out.println("PageNumber:" + this.pageNumber);
    System.out.println("RecordCount:" + this.recordCount);
    System.out.println("Offset:" + this.offset);
    System.out.println("Length:" + this.length);
  }
}
