package com.simplyindexedlogger.logger;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Spins up a thread to read data from a buffer and to write to destination files/pages (data & index)
 * Notifies listeners on exception
 * On exception, hands off buffer entries to data writer before exiting
 * @author arifkhan9
 *
 */

public class DestinationWriterThread extends Thread implements ExceptionCallback
{
	private List<ExceptionListener> listeners = new ArrayList<ExceptionListener>();
	private final Buffer buffer;
	private DataPageWriter dataHandler;
	private final String targetLocation;
	private UnixTimestamp currentTimestamp = null;

	public DestinationWriterThread(String targetLocation) throws IOException 
	{
		this.buffer = Buffer.getInstance();
		this.targetLocation = targetLocation;
	}
	
	private void setDataPageWriter() throws IOException
	{
	    UnixTimestamp timestamp = new UnixTimestamp();
	  // first write
	  if (this.dataHandler == null)
	  {
	    this.dataHandler = createDataPageWriter(); 
	    this.currentTimestamp = timestamp;
	  }
	  // if hour changes, close writers and creat new object
	  else
	  {
	    if(timestamp.getHour() != currentTimestamp.getHour())
	    {
	      // close current files
	      this.dataHandler.close();
	      this.currentTimestamp = timestamp;
	      this.dataHandler.close();
	      // create new files
	      this.dataHandler = createDataPageWriter();
	    }
	  }
	}
	
	private DataPageWriter createDataPageWriter() throws IOException
	{
	  return new DataPageWriter(this.targetLocation);
	}
	
	/**
	 * Reads data from shared buffer and hands it off to handler to write to data page & update index
	 * method called when process runs fine
	 * @throws InterruptedException
	 * @throws IOException
	 */
	private boolean processEntryFromSharedBuffer() throws InterruptedException, IOException
	{
	  if(buffer.Exists())
	  {
	    setDataPageWriter();
	    String dataItem = buffer.Remove();
	    dataHandler.putData(dataItem);
	    return true;
	  }
	  return false;
	}
	
	private void emptySharedBuffer()
	{
	  try 
	  {
	    while (processEntryFromSharedBuffer());
	  }
	  catch(Exception iex) {}
	}
	
	@Override
	public void run() 
	{
	  try 
	  {
	    while(true) 
	    {
	      while(!buffer.Exists()) 
	      {
	        synchronized(buffer.getLock()) 
	        {
	          buffer.getLock().wait();
	        }
	      }
	      processEntryFromSharedBuffer();
	    }
   }
   catch(Exception allEx) 
	 {
     notifyListeners(this.listeners, allEx);
   }
	 finally {
     emptySharedBuffer();
     try 
     {
       dataHandler.close();
     }
     catch(Exception ex) {}
	  }
	  
 }

	@Override
	public void addListener(ExceptionListener listener) 
	{
		this.listeners.add(listener);
	}

	@Override
	public void notifyListeners(Iterable<ExceptionListener> listeners, Exception occurredException) 
	{
		for(ExceptionListener ex: listeners) 
		{
			ex.exceptionOccurred(occurredException);
		}
	}	
}
